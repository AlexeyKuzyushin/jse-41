package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ru.rencredit.jschool.kuzyushin.tm.repository.ITaskRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.*;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectStartDateException;

import java.util.Date;
import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskRepository taskRepository;

    @Autowired
    public TaskService(
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskRepository taskRepository
    ) {
        this.userService = userService;
        this.projectService = projectService;
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Long count() {
        return taskRepository.count();
    }

    @Override
    @Nullable
    public Task getOneById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.getOne(id);
        return task;
    }

    @Override
    public void removeTaskById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteById(id);
    }

    @Override
    public @Nullable Task findTaskById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Task task = taskRepository.findById(id).get();
        return task;
    }

    @Override
    public void updateTaskById(@Nullable String id, @Nullable String name, @Nullable String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Task task = findTaskById(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return TaskDTO.toDTO(taskRepository.findAll());
    }

    @NotNull
    @Override
    public  List<TaskDTO> findAllByUserId(final @Nullable String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return TaskDTO.toDTO(taskRepository.findAllByUserId(userId));
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(final @Nullable String projectId) {
        return TaskDTO.toDTO(taskRepository.findAllByProjectId(projectId));
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String projectId,
                       final @Nullable String name, final @Nullable String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.findById(userId));
        task.setProject(projectService.findById(userId, projectId));
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public Task findById(final @Nullable String userId, final @Nullable String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Task findByName(final @Nullable String userId, final @Nullable String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByUserIdAndName(userId, name);
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    public void removeById(final @Nullable String userId, final @Nullable String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void removeByName(final @Nullable String userId, final @Nullable String name) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    public void removeAllByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteAllByProjectId(projectId);
    }

    @Nullable
    @Override
    public Task updateById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Task task = findById(userId, id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Override
    public void updateStartDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        task.setStartDate(date);
        taskRepository.save(task);
    }

    @Override
    public void updateFinishDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Task task = findById(userId, id);
        if (task.getStartDate() == null) throw new IncorrectStartDateException();
        if (task.getStartDate().after(date)) throw new IncorrectStartDateException(date);
        task.setFinishDate(date);
        taskRepository.save(task);

    }

    @Override
    public void load(final @Nullable List<TaskDTO> tasks) {
        if (tasks == null) return;
        taskRepository.deleteAll();
        for (final TaskDTO taskDTO: tasks){
            @NotNull final Task task = new Task();
            task.setName(taskDTO.getName());
            task.setDescription(taskDTO.getDescription());
            task.setUser(userService.findById(taskDTO.getUserId()));
            task.setProject(projectService.findProjectById(taskDTO.getProjectId()));
            task.setId(taskDTO.getId());
            task.setStartDate(taskDTO.getStartDate());
            task.setFinishDate(taskDTO.getFinishDate());
            task.setCreationTime(taskDTO.getCreationDate());
            taskRepository.save(task);
        }
    }
}
