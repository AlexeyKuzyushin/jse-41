<jsp:include page="../include/_header.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<h1>PROJECT CREATE</h1>

<form:form action="/projects/create" method="POST" modelAttribute="project">
    <p>
        <div style="margin-bottom: 5px">USER:</div>
        <select name="userId">
            <c:forEach items="${users}" var="user">
                <option value=<c:out value="${user.id}"/>><c:out value="${user.login}"/></option>
            </c:forEach>
        </select>
    </p>

    <p>
        <div style="margin-bottom: 5px">NAME:</div>
        <div><input type="text" name="name" value="${project.name}"/></div>
    </p>

    <p>
        <div style="margin-bottom: 5px">DESCRIPTION:</div>
        <div><input type="text" name="description" value="${project.description}"/></div>
    </p>
    <button type="submit">SAVE PROJECT</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />