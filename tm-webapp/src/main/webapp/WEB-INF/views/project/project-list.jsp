<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp" />

<h1>PROJECT LIST</h1>

<table width="100%" border="1" cellpadding="10" style="border-collapse: collapse">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="100" nowrap="nowrap">EDIT</th>
        <th width="100" nowrap="nowrap">DELETE</th>
    </tr>
    <c:forEach items="${projects}" var="project">
            <tr>
                <td><c:out value="${project.id}"/></td>
                <td><a href="/projects/view/${project.id}"><c:out value="${project.name}"/></a></td>
                <td><c:out value="${project.description}"/></td>
                <td style="font-size: 15px" align="center"><a href="/projects/update/${project.id}">UPDATE</a></td>
                <td style="font-size: 15px" align="center"><a href="/projects/delete/${project.id}">DELETE</a></td>
            </tr>
    </c:forEach>
</table>
<form action="/projects/create" style="margin-top: 10px;">
    <button border="1">CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp" />